# Ensure that in Windows you are running Git Bash

cd ~
mkdir mywebsite
cd mywebsite
git init

touch index.html
vi index.html

# Put basic structure of html page in file
<html>
<head>
  <title>My Website</title>
<head>
<body>
  <h1>Welcome to my website</h1>
  <a href="page2">Click here to see page 2</a>
</body>
</html>
# Press ESC and then SHIFT + Z + Z to save and quit

vi page2.html

# Basic page structure
<html>
<head>
  <title>My Website</title>
<head>
<body>
  <h1>Welcome to Page 2</h1>
  <a href="index">Click here to see the home page</a>
</body>
</html>
# Press ESC and then SHIFT + Z + Z to save and quit

git add .
git commit -m "Initial structure added"

git branch dev
git checkout dev

vi page3.html
# Basic page structure
<html>
<head>
  <title>My Website</title>
<head>
<body>
  <h1>Welcome to Page 3</h1>
</body>
</html>
# Press ESC and then SHIFT + Z + Z to save and quit

git add page3.html
git commit -m "page3 added"

# Test site in browser...errors with links
# Need to hotfix!
git checkout master
git branch hotfix
git checkout hotfix

vi index.html
# Fix the link by adding '.html' to the end of the href
# Press ESC and then SHIFT + Z + Z to save and quit

vi page2.html
# Fix the link by adding '.html' to the end of the href
# Press ESC and then SHIFT + Z + Z to save and quit

git add .
git commit -m "hrefs fixed to link to correct file"
# Test that it is working

# Merge hotfix into master
git checkout master
git merge hotfix
git status

# Merge hotfix to dev branch
git checkout dev
git merge hotfix
git status

# Delete the hotfix branch
git branch -d hotfix
git branch
